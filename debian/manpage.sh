#!/bin/bash

help2man hey --no-info --no-discard-stderr --version-string="$(dpkg-parsechangelog -S Version | cut -d- -f1)" -n "HTTP load generator, formerly known as rakyll/boom" > debian/hey.1
